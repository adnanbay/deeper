const express = require('express');
const app = express()
const port = 3000
const userRouter = require('./users/users.route');
const gameRoute = require('./game/game.route');

app.use(express.json());
app.use(express.static("public"));

app.get("/", (req, res) => {
  return res.sendFile("index.html");
})

app.use("/users", userRouter);
app.use("/games", gameRoute);

app.get('/ping', (req, res) => {
  res.send('pong')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})