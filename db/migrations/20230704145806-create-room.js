'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      roomName: {
        type: Sequelize.STRING
      },
      player1Id: {
        type: Sequelize.INTEGER,
        references: {
          model : "Users",
          key: "id",
        },
      },
      player2Id: {
        type: Sequelize.INTEGER,
        references: {
          model : "Users",
          key: "id",
        },
      },
      pilihanP1: {
        type: Sequelize.STRING
      },
      pilihanP2: {
        type: Sequelize.STRING
      },
      pemenang: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Rooms');
  }
};