'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Game.belongsTo(models.User, {foreignKey: "playerId"});
      // Game.belongsTo(models.User, {foreignKey: "lawanId"});
      Game.belongsTo(models.Room, {foreignKey: "roomId"});
    }
  }
  Game.init({
    playerId: DataTypes.INTEGER,
    pilihan: DataTypes.STRING,
    lawanId: DataTypes.INTEGER,
    hasil: DataTypes.STRING,
    roomId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Game',
  });
  return Game;
};