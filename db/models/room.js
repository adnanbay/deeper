'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Room.hasOne(models.Game, {foreignKey: "roomId"});
      Room.belongsTo(models.User, {foreignKey: "player1Id", as: "player1"});
      Room.belongsTo(models.User, {foreignKey: "player2Id", as: "player2"});
    }
  }
  Room.init({
    roomName: DataTypes.STRING,
    player1Id: DataTypes.INTEGER,
    player2Id: DataTypes.INTEGER,
    pilihanP1: DataTypes.STRING,
    pilihanP2: DataTypes.STRING,
    pemenang: DataTypes.STRING,
    status: DataTypes.STRING,
    hasilP1:DataTypes.STRING,
    hasilP2:DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Room',
  });
  return Room;
};