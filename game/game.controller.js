const judgementValidation = require("../schema/judgementValidation");
const userModel = require("../users/users.model");
const gameModel = require("./game.model");

class GameController {

    getAllgames = (req, res) => {
        return res.send("Hello from game controller");
    }

    createRoom = async (req,res) => {

      const idUser = req.token.id
      // ambil data di body
      const {namaRoom, pilihan} = req.body
      // cek ada nama roomnya apa ngga, harus isi nama

      if (namaRoom === undefined || namaRoom === ""){
        res.statusCode= 400;
        return res.json({message: "Harus input nama room"})
      }
      if (pilihan === undefined || pilihan === ""){
        res.statusCode= 400;
        return res.json({message: "Harus input pilihan! (rock/paper/scissor)"})
      }

      // cek nama room, harus beda 

      const existRoomName = await gameModel.isRoomNameExist(namaRoom);

      if(existRoomName){
          return res.json({message: "Room Name udah ada, cari nama lain"});
      }
        
      try {
        // input room ke db
        gameModel.createRoom(idUser, namaRoom, pilihan);
        res.statusCode = 201;
        return res.json({message: "Room sudah dibuat"})
        
      } catch (error) {
        res.statusCode = 400;
        return res.json({message: "Error DB"})
      }

    };

    getAllRooms = async (req, res) => {
      // const idUser = req.token.id
      const allRooms = await gameModel.getAllRooms();
      res.json(allRooms);
    }

    joinRoom = async (req, res) => {
      
      // ambil id user dari token
      const idUser = req.token.id
      // ambil id room dari params
      const {idRoom} = req.params
      // ambil pilihan dari body
      const {pilihanP2} = req.body

      try {
        
      // cek pilihan di body harus rock paper scissor
      if(pilihanP2 !== "rock" &&
         pilihanP2 !== "paper" &&
         pilihanP2 !== "scissor"
      ){
        res.statusCode= 400;
        return res.json({message: "Format pilihan harus rock/paper/scissors"})
      }

      // ambil id room nya
      const singleRoom = await gameModel.getSingleRoom(idRoom);

      // console.log(singleRoom.player1Id);
      // console.log(singleRoom.status);
      // cek idroomnya, player2 ga boleh lawan room sendiri
      if (singleRoom.player1Id === idUser){
        res.statusCode = 400;
        return res.json({message: "Ini adalah Room yang ada buat, silahkan ganti room lain"})
      }

      if (singleRoom.status === "complete"){
        res.statusCode = 400;
        return res.json({message: "Permainan ini telah selesai,  silahkan ganti room lain"})
      }
      // update ROOM beserta dengan id dan pilihan player 2
      await gameModel.joinRoom(pilihanP2, idUser, idRoom);

      // ambil pilihan player 1 

      const pilihanP1 = singleRoom.pilihanP1;
      
      console.log(pilihanP1);
      console.log(pilihanP2);
      // Menentukan pemenang
      const result = judgementValidation.judgementRPS(pilihanP1, pilihanP2);

      console.log(result);

      // // Update Room untuk menambahkan hasil

      const roomResult = await gameModel.roomResult(result, idRoom);
      res.json({message : 
        "Pertandingan Selesai",
        "Pilihan Player 1 ": pilihanP1, 
        "Pilihan Player 2 ": pilihanP2, 
        "Player 1 Result ": result[0], 
        "Player 2 Result ": result[1],
        "Room id ": idRoom 
      })

      return roomResult;

    } catch (error) {
      console.log(error);
      return res.status(400).json({ message: "Ada kesalahan di Controller" });
    }
    }

    getAllHistories = async (req, res) => {
      // ambil id user dari token
      const idUser = req.token.id

      try {

        const users = await gameModel.getAllHistories(idUser);

        if(users){
          return res.json(users);
        } else {
          res.statusCode= 400;
          return res.json({message: "Data " + idUser + " tidak ditemukan ga ada"});

        }
      } catch (error) {
        console.log(error)
        res.statusCode=400;
        return res.json({message: "Data " + idUser + " tidak ditemukan kenapa"});
      }

    }

    recordGame = (req, res) => {

        // ambil data body
        const { playerId, pilihan, lawanId, hasil, roomId } = req.body;

        // insert ke db game
        gameModel.recordGame(playerId,pilihan,lawanId,hasil,roomId);

        return res.json({message: " Berhasil record Game"})
    }

    getHistories = async (req, res) => {
        const {idUser} = req.params;
        
        try {

        const users = await gameModel.getHistories(idUser);

        if(users){
          return res.json(users);
        } else {
          res.statusCode= 400;
          return res.json({message: "Data " + idUser + " tidak ditemukan ga ada"});

        }
      } catch (error) {
        console.log(error)
        res.statusCode=400;
        return res.json({message: "Data " + idUser + " tidak ditemukan kenapa"});
      }
    };
}

module.exports = new GameController;