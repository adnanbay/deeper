const db = require('../db/models');
const { Op } = require('sequelize');

class GameModel {

    recordGame =  (playerId, pilihan, lawanId, hasil, roomId) => {
        db.Game.create({
            playerId,
            pilihan,
            lawanId,
            hasil,
            roomId
        });
    };


    isRoomNameExist = async (namaRoom) => {
        const existData = await db.Room.findOne({
            where: {roomName:namaRoom},
        })

        if (existData) {
            return true;
        }else {
            return false;
        }
    }

    // Buat Room player vs player ( Record id dan pilihan Player 1)
    createRoom = (idUser, namaRoom, pilihan) => {
        db.Room.create({
            roomName: namaRoom,
            player1Id: idUser,
            pilihanP1: pilihan,
            status: "available",
        });
    }

    // Single room (dapetin id Room)
    getSingleRoom = async (idRoom) => {
        const singleRoom = await db.Room.findOne({
            where: { id : idRoom}
        });

        return singleRoom;
    }

    // Record id dan pilihan vs Player 2 di Room
    joinRoom = (pilihanP2, idUser, idRoom) => {
        db.Room.update({
            pilihanP2 : pilihanP2,
            player2Id: idUser
        },
        { where: {id : idRoom}}
        );
    }

    roomResult = async (result, idRoom) => {
        await db.Room.update(
            {
            hasilP1: result[0],
            hasilP2: result[1],
            status: "complete"
            },
            { where: {id:idRoom}}
        );
    };

    getAllRooms = async () => {
        const allRooms = await db.Room.findAll({
            attributes: {
                exclude: ["player2Id","pilihanP1","pilihanP2", "pemenang", "hasilP1", "hasilP2", "createdAt", "updatedAt" ]
            },
            include: [
                {
                model: db.User, 
                as: "player1",
                attributes: {
                    exclude: ["id", "email", "password", "createdAt", "updatedAt"]
                    }
                },
                {
                model: db.User, 
                as: "player2",
                attributes: {
                exclude: ["id", "email", "password", "createdAt", "updatedAt"]
                    }
                },
            ]
        });
        return allRooms;
    }

    getAllHistories = async (idUser) => {
        
        try {        
        
        const roomList =  await db.Room.findAll({
            attributes: [
                "roomName",
                "updatedAt",
                "player1Id",
                "player2Id",
                "hasilP1",
                "hasilP2",
            ],
            where: {
                [Op.or]: [{ player1Id: idUser }, { player2Id: idUser }],
                [Op.and]: [
                    {hasilP1: {[Op.ne]: null}},
                    {hasilP2: {[Op.ne]: null}},
                ],
              },
            raw: true,
        });

        return roomList.map((room) => ({
            playerId: room.player1Id === idUser ? room.player1Id : room.player2Id,
            hasilPlayer: room.player1Id === idUser ? room.hasilP1 : room.hasilP2,
            roomName: room.roomName,
            updatedAt: room.updatedAt,
        }));

        } catch (error) {
            throw new Error("Gagal menerima room detail : " + error.message);
         }
    }
    
    //method ambil histories dari db user
    getHistories = async (idUser) => {
        return await db.User.findOne({
            attributes: {
                exclude: ["password", "createdAt", "updatedAt"],
            },
            include: [
                {
                model: db.Game,
                // as: "playerId",
                attributes: {
                    exclude: [
                        "id","playerId","updatedAt","lawanId","pilihan"],
                    },
                },
                {
                model: db.Game,
                // as: "lawanId",
                attributes: {
                    exclude: [
                        "id"],
                        },
                    },
            ],
            where: {id:idUser}
        });
    }
    
}

module.exports = new GameModel();