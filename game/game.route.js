const express = require('express');
const gameController = require('./game.controller');
const gameRoute = express.Router();
const authMiddleware = require('../middleware/authMiddleware');


gameRoute.get('/', gameController.getAllgames);

gameRoute.post('/create-room',authMiddleware, gameController.createRoom);

gameRoute.get('/room',authMiddleware, gameController.getAllRooms);

gameRoute.put('/join-room/:idRoom', authMiddleware, gameController.joinRoom);

gameRoute.get('/getAllHistories', authMiddleware, gameController.getAllHistories);

// gameRoute.post('/recordGame', gameController.recordGame);

// gameRoute.get('/gethistories/:idUser', gameController.getHistories)


module.exports = gameRoute;