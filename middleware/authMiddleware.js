const jwtTool = require('jsonwebtoken');

const authMiddleware = async (req, res, next) => { 
    // cek ada auth ga di header
    const authorization = req.headers.authorization;
    // jika auth undifined atau null maka data direject

    if (authorization=== undefined) {
      res.statusCode = 400;
      return res.json({message: "Unauthorized! Ga ada authnya kamu hey"})
    }

    // cek apakah authorization valid, kalau tidak valid return Unauthorized

    try {
      // ambil token dari header, trus di bagi tokennya

      const token = authorization.split(" ")[1];

          
      const devideToken = await jwtTool.verify(token, "cobaKUNCIjwt");
      req.token = devideToken;
      next();
    } catch (error) {
      res.statusCode= 400;
      return res.json({message:"Unauthorized auth"})
    }
};

module.exports = authMiddleware;