const judgementRPS = (choiceOne, choiceTwo) => {
    if (
      (choiceOne === "rock" && choiceTwo === "rock") ||
      (choiceOne === "scissor" && choiceTwo === "scissor") ||
      (choiceOne === "paper" && choiceTwo === "paper")
    ) {
      return ["draw", "draw"];
    } else if (
      (choiceOne === "rock" && choiceTwo === "scissor") ||
      (choiceOne === "scissor" && choiceTwo === "paper") ||
      (choiceOne === "paper" && choiceTwo === "rock")
    ) {
      return ["win", "lose"];
    } else if (
      (choiceOne === "rock" && choiceTwo === "paper") ||
      (choiceOne === "scissor" && choiceTwo === "rock") ||
      (choiceOne === "paper" && choiceTwo === "scissor")
    ) {
      return ["lose", "win"];
    } else {
      return ["error", "error"];
    }
  };
  
  module.exports = { judgementRPS };