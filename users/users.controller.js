const userModel = require('./users.model');
const jwtTool = require('jsonwebtoken');

class UserController {
    getAllUsers  = async (req, res) => {
        const allUsers = await userModel.getAllUsers();
        res.json(allUsers);
    }

    getSingleUser = async (req, res) => {
      const idUser = req.token.id

      try {

        const users = await userModel.getSingleUser(idUser);

        if(users){
          return res.json(users);
        } else {
          res.statusCode= 400;
          return res.json({message: "Data " + idUser + " tidak ditemukan"});

        }
      } catch (error) {
        res.statusCode=400;
        return res.json({message: "Data " + idUser + " tidak ditemukan"});
      }
    };

    updateUserBio = async (req,res) => {

      const { idUser} = req.params;
      
      try {
        const {fullname, address, phoneNumber} = req.body;
        
        // check user bio dah ada apa belum
        const existUser = await userModel.isUserBioExist(idUser);
  
        // ada update
        if(existUser) {
          userModel.updateUserBio(idUser, fullname, address, phoneNumber);
        } else {
          // belum ada create
          await userModel.creteUserBio(idUser, fullname, address, phoneNumber);
        }
  
        return res.json({message: "Data sudah di update"})
        // const updateUser = await userModel.updateUserBio(idUser, fullname, address, phoneNumber);
        // return res.json(updateUser);
        
      } catch (error) {
        res.statusCode=400;
        return res.json({message: "Data " + idUser + " tidak ditemukan" });
      }
    }

    registerUsers = async (req, res) => {
        const dataReq = req.body;

        // check user atau email udah ada apa belum
      
        const existData = await userModel.isUserRegistered(dataReq);
      
        if(existData) {
          return res.json({message: "Username or Email is already registered"});
        }
      
        // record data User
        userModel.recordDataUser(dataReq);
        res.json({message: "Sukses menambahkan user baru"});
    }

    loginUsers = async (req, res) => {
        const {username, password} = req.body;
  
    // CEK dah isi user sama pass atau belum
    if(username === undefined || username === "") {
      res.statusCode = 400;
      return res.json({message: "Masukan username"});
    }
    if(password === undefined || password === "") {
      res.statusCode = 400;
      return res.json({message: "Masukan Password"});
    };
    
    // Verify Login
    const dataLogin = await userModel.verifyLogin(username, password);

    // delete dataLogin.password;
    
    // kalau user valid
    if (dataLogin) {
      // generate JWT
      const token = jwtTool.sign(
        {...dataLogin, role:"player"}, 
        "cobaKUNCIjwt", 
        {expiresIn: "1d"});

      return res.json({accessToken: token});
    } else {
      res.statusCode = 404;
      return res.json({message: "Username atau Password salah"});
    };
    };

    recordGame = async (req, res) => {
      // dapat id user dari params
      const {idUser} = req.params;

      // dapat status dari body
      const {status} = req.body;
    // check userid ada apa ngga di db

    const playerId = await userModel.isUserExist(idUser);
    if(!playerId){
      res.statusCode = 404;
      return res.json({message: "ID tidak ada"})
    }

    if(status === undefined || status === "") {
      res.statusCode = 400;
      return res.json({message: "Input status (win/lose)"});
    };

    // record data Game
    userModel.recordDataGame(idUser, status);
    return res.json({message: "Sukses menambahkan data Game"});

    }

    gameHistory = async (req, res) => {
      const {idUser} = req.params;
      
      try {

        const users = await userModel.gameHistory(idUser);

        if(users){
          return res.json(users);
        } else {
          res.statusCode= 400;
          return res.json({message: "Data " + idUser + " tidak ditemukan"});

        }
      } catch (error) {
        res.statusCode=400;
        return res.json({message: "Data " + idUser + " tidak ditemukan"});
      }
    }
}

module.exports = new UserController();