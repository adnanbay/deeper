// let userList = [];
const md5 = require('md5');
const db = require('../db/models');
const { Op } = require('sequelize');
class UserModel {

    // method untuk dapat semua user
    getAllUsers  = async () => {
        const dataUsers = await db.User.findAll({include: [db.UserBio, db.GameHistory]});
        return dataUsers;
    };

    getSingleUser = async (idUser) => {
        return await db.User.findOne({
            attributes: { exclude: ["createdAt", "updatedAt"]},
            include: [{
                model :db.UserBio,
                attributes: { 
                    exclude: ["id", "createdAt", "updatedAt"]
                }
            }],
            where: { id: idUser}});
    };

    // method cek user bio ada apa ngga
    isUserBioExist = async (idUser) => {
        const existData = await db.UserBio.findOne({
            where: {userId:idUser},
        })

        if (existData) {
            return true;
        }else {
            return false;
        }
    }

    // method create user bio
    creteUserBio = async (idUser, fullname, address, phoneNumber) => {
        return await db.UserBio.create({
            fullname,
            address,
            phoneNumber,
            userId: idUser,
        });
    };

    updateUserBio = async (idUser, fullname, address, phoneNumber) => {
        return await db.UserBio.update(
            {fullname: fullname, address:address, phoneNumber:phoneNumber},
            {where: { userId: idUser}});
    };

    // method check user udah ada apa belum

    isUserRegistered = async (dataReq) => {
        const existData = await db.User.findOne({
            where: {[Op.or]: [
                { username: dataReq.username},
                { email: dataReq.email},
            ],
        },
        })
        
        // const existData = userList.find((data) => {
        // return (data.username === dataReq.username || data.email === dataReq.email);
        // });

        if (existData) {
            return true;
        }else {
            return false;
        }
    }

    //method record new data User
    recordDataUser = (dataReq) => {
        db.User.create({
            username: dataReq.username,
            email: dataReq.email,
            password: md5(dataReq.password),
          });
    }
    

    isUserExist = async (idUser) => {
        const playerId = await db.User.findOne({
            where: { id : idUser},
        });

        if (playerId) {
            return true;
        } else {
            return false;
        }
    };

    //method new record Data Game
    recordDataGame = (idUser, status) => {
        db.GameHistory.create({
            userId: idUser,
            status: status
        });
        
    }

    

    //method ambil gamehistory dari db user
    gameHistory = async (idUser) => {
        return await db.User.findOne({ 
            include: [db.GameHistory],
            where: {id:idUser}});
    }

    // Verify Login
    verifyLogin = async (username, password) => {
        const sortedValue = await db.User.findOne({
                where: {
                    username:username, 
                    password:md5(password)},
                    attributes: {exclude: ['password']}, 
                    raw: true
            });
        
        // const sortedValue = userList.find((data) => {
    
        //     return (
        //       data.username === username && 
        //       data.password === md5(password)
        //       );
            
        //   });

        return sortedValue;
    }
};

module.exports = new UserModel();