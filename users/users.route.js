const express = require('express');
const userRouter = express.Router();
const UserController = require('./users.controller');
const authMiddleware = require('../middleware/authMiddleware');
const protection = require('../middleware/protectionMiddleware');
const {checkSchema} = require('express-validator');
const schemaValidation = require('../middleware/schemaValidation');
const { registrationSchema } = require('./users.schema');


// API untuk lihat user
userRouter.get('/',authMiddleware, UserController.getAllUsers);

//API untuk register user baru
userRouter.post('/registration', 
    checkSchema(registrationSchema), 
    schemaValidation,
    UserController.registerUsers);

// API LOGIN
userRouter.post('/login', UserController.loginUsers);

// API cari detail user
userRouter.get('/detail/', authMiddleware,UserController.getSingleUser);

// API detail user bio
userRouter.put('/detail/:idUser', authMiddleware, protection, UserController.updateUserBio);

// API record game
userRouter.post('/recordgame/:idUser', authMiddleware, UserController.recordGame);

// API liat game history per User
userRouter.get('/gamehistory/:idUser', UserController.gameHistory);

module.exports = userRouter;