const registrationSchema = {
    username: {isLength: {options: {min:1}}},
    email: {isEmail: true, errorMessage: "Isi email dengan benar"},
    password: { isLength: {options: {min:1}}, errorMessage: "password kurang dari 5"},
};

const loginSchema = {
    email: {isEmail: true, errorMessage: "Isi email dengan benar"},
    password: { isLength: {options: {min:1}}, errorMessage: "password kurang dari 5"},
};

module.exports = {
    registrationSchema,
    loginSchema,
};